function numberSquare(array) {
   let square_array = []
   for(let index = 0; index < array.length; index++) {
	square_array.push(array[index] ** 2);
   }
   return square_array;
}

function printArray(array) {
   for(let index = 0; index < array.length; index++) {
	console.log(array[index]);
   }
}

module.exports = {numberSquare,printArray};
